package uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.solvers;

import java.util.ArrayList;
import java.util.Random;

import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.ExploredSpot;
import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.MineMap;
import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.Spot;

public class AISolver extends AbstractSolver {
    public void run() {
        MineMap map = getMyMap();
        int tileChange = 0;
        RandomTile(tileChange, map);
        while(!map.isEnded() || !map.isWon()) {
            for (int row = 0; row < map.rows; row++) {
                for (int col = 0; col < map.cols; col++) {
                    // Gets position coordinates
                    ExploredSpot exploredSpot = map.getPos(row, col);
                    // Checks if tile is safe and there is at least 1 mine nearby
                    if (Spot.SAFE.equals(exploredSpot.type) && exploredSpot.nearMineCount > 0) {
                        // Gets data of all tiles in a 3x3 around the tile checked
                        TileMineLocations tilesData = checkTilesAround(row, col, map);
                        // System.out.println("Current Coordinates - Row: " + row + " Column: " + col);
                        // System.out.println("Size of Coordinates Array: " + tilesData.getCoordinates().size() + " Nearby Mine Count: " + exploredSpot.nearMineCount + " Unexplored Tiles Count: " + tilesData.getCount());
                        if ((tilesData.getCount() + tilesData.getFlag()) == exploredSpot.nearMineCount) {
                            tileChange++;
                            flagTiles(tilesData.getCoordinates(), map);
                        } else if (tilesData.getFlag() == exploredSpot.nearMineCount) {
                            tileChange++;
                            exploreTiles(tilesData.getCoordinates(), map);
                        } else {
                            tileChange = 0;
                        }
                    }
                }
            }
            if(tileChange == 0) {
                RandomTile(tileChange, map);
            }
        }
    }

    public void RandomTile (int tileChange, MineMap map) {
        Random number = new Random();
        if (tileChange == 0) {
            while(true) {
                int ranRow = number.nextInt(map.rows);
                int ranCol = number.nextInt(map.cols);
                ExploredSpot exploredSpot = map.getPos(ranRow, ranCol);
                if(Spot.UNEXPLORED.equals(exploredSpot.type)) {
                    System.out.println("Picking a new random position at: " + ranRow + " - " + ranCol);
                    map.pickASpot(ranRow, ranCol);
                    return;
                }
            }
        }
    }

    public TileMineLocations checkTilesAround (int row, int col, MineMap map) {
        int countUnexplored = 0;
        int currentFlags = 0;
        ArrayList<Coordinates> coordinates = new ArrayList<>();
        for(int curRow = row - 1; curRow <= row + 1; curRow++) {
            for(int curCol = col - 1; curCol <= col + 1; curCol++) {
                if(curRow < 0 || curCol < 0 || curRow >= map.cols || curCol >= map.rows) break;
                ExploredSpot exploredSpot = map.getPos(curRow, curCol);
                // System.out.println("Exploring - Row: " + curRow + " Column: " + curCol);
                if(Spot.FLAG.equals(exploredSpot.type)) {
                    currentFlags++;
                }
                else if(Spot.UNEXPLORED.equals(exploredSpot.type)) {
                    countUnexplored++;
                    coordinates.add(new Coordinates(curRow, curCol));
                }
            }
        }

        return new TileMineLocations(countUnexplored, currentFlags, coordinates);
    }

    public void flagTiles (ArrayList<Coordinates> coordinatesList, MineMap map) {
        for(Coordinates coordinates : coordinatesList) {
            map.flagASpot(coordinates.getRow(), coordinates.getColumn());
        }
    }

    public void exploreTiles (ArrayList<Coordinates> coordinatesList, MineMap map) {
        for(Coordinates coordinates : coordinatesList) {
            map.pickASpot(coordinates.getRow(), coordinates.getColumn());
        }
    }
}
