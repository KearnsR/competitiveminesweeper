package uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.solvers;

public class Coordinates {
    private Integer row;
    private Integer column;

    public Coordinates(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }
}
