package uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.solvers;

import java.util.ArrayList;

public class TileMineLocations {
    private Integer count;
    private Integer flag;
    private ArrayList<Coordinates> coordinates;

    public TileMineLocations(int count, int flag, ArrayList<Coordinates> coordinates) {
        this.count = count;
        this.flag = flag;
        this.coordinates = coordinates;
    }

    public int getCount() {
        return count;
    }

    public int getFlag() { return flag; }

    public ArrayList<Coordinates> getCoordinates() {
        return coordinates;
    }
}
